(* ::Package:: *)

(* ::Title:: *)
(*PolyLogTools*)


(* ::Subsection:: *)
(*Print out*)


PLT$Version = "1.4";


Print["(****** PolyLogTools ", PLT$Version, " ******)"];
Print["    Authors: Claude Duhr, Falko Dulat"];
Print["    Email: cduhr@uni-bonn.de"];
Print["    PolyLogTools uses the implementation of the PSLQ algorithm by P. Bertok (http://library.wolfram.com/infocenter/MathSource/4263/)"];


Off[General::compat];

BeginPackage["PolyLogTools`", {"HPL`"}];

If[$VersionNumber < 13.1,
    Needs["Combinatotica`"];
];


On[General::compat];

If[ !ValueQ[Global`$PolyLogPath],	
	$PolyLogToolPath = DirectoryName[$InputFileName],
	$PolyLogToolPath = Global`$PolyLogPath;
];

GiNaC$InterfacePath = $PolyLogToolPath <> "/Temp/";


(* ::Subsection:: *)
(*Flags*)


PLT$AutoConvertToKnownConstants = True;
PLT$ShuffleRegularisation = True;

PLT$MaxDenominatorReductionVariables = 11;

PLT$Verbose = False;

PLT$ginsh = "ginsh";


Unprotect[HPL]
HPL[{-1},-1]=-HPL[{1},1]
Protect[HPL]

Get[FileNameJoin[{$PolyLogToolPath, "Usage.m"}]];

$QExtension = {};
$Alphabet = {0,1,-1};
$AlphabetBound = 5;

Begin["`Private`"];

(* ::Subsection:: *)
(*Format*)

vPrint[x___]:=If[PLT$Verbose,Print[x]];

Format[Delta, StandardForm] = \[CapitalDelta];
Format[Delta, TraditionalForm] = \[CapitalDelta];

Format[CT[as__], StandardForm] := CircleTimes[as];
Format[CT[as__], TraditionalForm] := CircleTimes[as];


Format[Cl[n_,x_],TraditionalForm] := Subscript[Cl,n][x]
Format[Cl[n_,x_],StandardForm] := Subscript[Cl,n][x]


Format[CTW[a_,b_],TraditionalForm]:=Wedge[a,b];
Format[CTW[a_,b_],StandardForm]:=Wedge[a,b];


Format[PolyGon[verts__],TraditionalForm] := "["<>(StringJoin@@Riffle[ToString[#,TraditionalForm]&/@{verts},","])<>"]"
Format[PolyGon[verts__],StandardForm]    := Format[PolyGon[verts],TraditionalForm];


Format[CiTi[as___], StandardForm] := CircleTimes[as];
Format[CiTi[as___], TraditionalForm] := CircleTimes[as];


Format[Tens[x__],StandardForm]:=CiTi[x]
Format[Tens[x__],TraditionalForm]:=CiTi[x]

Format[TensWedge[x__],StandardForm]:=Wedge[x];
Format[TensWedge[x__],TraditionalForm]:=Wedge[x];

Format[ElTens[x__],StandardForm]:=x
Format[ElTens[x__],TraditionalForm]:=x


(* ::Subsection:: *)
(*Subdirectory structure and subroutines*)


Block[{

    $Path = ($PolyLogToolPath <> # &) /@ {"", "/lib", "/Temp", "/core"}

    },
    Get["Coproduct`"];
    Get["Regularization`"];
    Get["ShuffleAlgebra`"];
    Get["PolyLogToolBox`"];
    Get["Gint`"];
	Get["GiNaCInterface`"];
    Get["LyndonWords`"];
    Get["Symbols`"];
    Get["IntegrationSymbols`"];
    Get["CanonicalIntegrator`"];
    Get["Reduction`"];
    Get["PSLQ`"];
    Get["DenominatorReduction`"];
	Get["FitReduction`"];
	Get["PeriodMatrix`"];

    Get["Coproduct8`"];
    Get["Coproduct12`"];
    Get["SpecialPolyLogIdentities`"];
    Get["G12`"];
    Get["G13`"];
    Get["Gm12`"];
    Get["Gm13`"];
    Get["cLi.m"];
	Get["cLL.m"];
    Get["HPLBasis2`"];
    Get["HPLBasis3`"];
    Get["HPLBasis4`"];
    Get["HPLBasis5`"];
    Unprotect[MZV];
    Get["MZV7`"];
    Get["MZV8`"];
    Protect[MZV];
    Get["sixrootunity`"];

   (* Get["CosmicSVGs`"];*)

];


(* ::Subsection:: *)
(*Test if Ginsh is running*)


Ginsh::NotFound = "Warning: PolyLogTools was unable to automatically locate the ginsh excecutable!
Make sure that GiNaC and ginsh are installed properly or set PLT$ginsh variable to the full path to ginac binary.
Some functionalities will not work properly unless ginsh is properly running.";


If[Run["which "<>PLT$ginsh] =!= 0,
   Message[Ginsh::NotFound],
    Enclose[With[
        {file = Confirm[CreateFile[]]},
        Run["which "<>PLT$ginsh <> " >> "<>file];
        Echo["ginsh binary found at: "<>(Information[File[Import[file,"Text"]]]["AbsoluteFileName"])<>"\n(override by setting PLT$ginsh to full path)"]; 
        DeleteFile[file];
    ]];
];
    


(* ::Subsection:: *)
(**)

End[];

EndPackage[];
